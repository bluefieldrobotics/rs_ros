#!/usr/bin/env python3

import os, sys, time
import rospy
import serial
import re
import struct
import threading
from sensor_msgs.msg import JointState
from blueprintlab_reachsystem_ros_messages.msg import single_float
from blueprintlab_reachsystem_ros_messages.msg import single_int
from blueprintlab_reachsystem_ros_messages.msg import generic
from blueprintlab_reachsystem_ros_messages.msg import request_list
from RS1_hardware import PacketID
from packetid import Packets



class MoveIt_To_Reach5Mini():
	instances = []

	def __init__(self, r5m_instance=0, device_ids=[1,2,3,4,5], **kwargs):
		self.r5m_instance = r5m_instance
		self.device_ids = 	device_ids
		self.r5m_positions = [0] * len(self.device_ids)

		for r5m_inst in MoveIt_To_Reach5Mini.instances:
			if r5m_inst.r5m_instance == self.r5m_instance:
				raise Exception('Cannot use the same r5m_instance number as other MoveIt_To_Reach5Mini class instances!')
		MoveIt_To_Reach5Mini.instances.append(self)

		self.r5m_position_sub =     					rospy.Subscriber('r5m_' + str(self.r5m_instance) + '/position', single_float, self.r5m_position_callback)    
		self.r5m_cmd_position_pub = 					rospy.Publisher('r5m_' + str(self.r5m_instance) + '/cmd_position', single_float, queue_size=10)
		self.receive_simulated_joint_positions_sub = 	rospy.Subscriber('r5m_' + str(self.r5m_instance) + '/sim_desired_positions', JointState, self.receive_simulated_joint_positions_callback) 
		self.send_joint_positions_to_sim_pub = 			rospy.Publisher('r5m_' + str(self.r5m_instance) + '/r5m_positions', JointState, queue_size=10) 

		self.r5m_cmd_position_msg = single_float()
		self.send_joint_positions_to_sim_msg = JointState()

	'''
	Send positions to simulator loop
	'''
	def start(self):
		self.thread_request_loop = threading.Thread(target=self.send_positions_to_sim_loop)
		self.thread_request_loop.setDaemon(True)
		self.thread_request_loop.start()

	def send_positions_to_sim_loop(self):
		send_frequency = 10.0
		while not rospy.is_shutdown():
			self.send_joint_positions_to_sim_msg.header.stamp = rospy.get_rostime()
			self.send_joint_positions_to_sim_msg.name = []
			self.send_joint_positions_to_sim_msg.position = []
			self.send_joint_positions_to_sim_msg.velocity = []
			self.send_joint_positions_to_sim_msg.effort = []			
			for i, r5m_position in enumerate(self.r5m_positions):
				if i >= len(self.device_ids):
					break
				self.send_joint_positions_to_sim_msg.name.append(str(self.device_ids[i]))
				self.send_joint_positions_to_sim_msg.position.append(r5m_position)
			self.send_joint_positions_to_sim_pub.publish(self.send_joint_positions_to_sim_msg)
			time.sleep(1.0/send_frequency)

	'''
	ROS Message Callback Functions
	'''
	def r5m_position_callback(self, message):
		# Get joint position from R5M
		device_id = message.device_id
		value = message.value


	def receive_simulated_joint_positions_callback(self, message):
		# Receive all simulated positions and send values to R5M
		for i, sim_position in enumerate(message.position):
			if i >= len(self.r5m_positions):
				return
			device_id = self.device_ids[i]		
			self.r5m_cmd_position_msg.stamp = rospy.get_rostime()	
			self.r5m_cmd_position_msg.device_id = device_id
			self.r5m_cmd_position_msg.value = 1.0 * sim_position 	# Some reference conversion to be added here (sim axis position to r5m axis position)
			self.r5m_cmd_position_pub.publish(self.r5m_cmd_position_msg)
